module.exports = {
    inputQuote: '#inputQuote',
    selectColor: '#colorSelect',
    buttonSubmit: '#buttonAddQuote',
    textQuote: '.container div:nth-of-type(2) [name="quoteText"]',
    colorQuote: '.container div:nth-of-type(2) .panel-default'
}